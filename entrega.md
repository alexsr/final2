#ENTREGA CONVOCATORIA JUNIO

Erik Alejandro Sánchez Romero

Correo: ea.sanchez.2023@alumnos.urjc.es

Enlace del vídeo: https://youtu.be/k4cItFUZ_o0

Requisitos mínimos:
transform.py (métodos mirror, grayscale, blur, change_colors, rotate, shift, crop y filter) , transform_simple.py, transform_args.py, transform_multi.py

Requisitos opcionales propios:
efectoruido.py