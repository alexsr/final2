import sys
from transforms import mirror, rotate, change_colors, blur, grayscale, shift, crop, filter
from images import read_img, write_img
def main():
    image_trans = read_img(sys.argv[3])
    parametros = sys.argv[4::]
    i = 0

    while i < len(parametros):
        if parametros[i] == "mirror":
            image_trans = mirror(image_trans)
            i+= 1
        if parametros[i] == "rotate":
            if parametros[i + 1] == 'left':
                    image_trans = rotate(image_trans, 'left')
                    i+= 2
            else:
                    image_trans = rotate(image_trans, 'right')
                    i+= 2
        elif parametros[i] == "blur":
            image_trans = blur(image_trans)
            i+= 1
        elif parametros[i] == "grayscale":
            image_trans = grayscale(image_trans)
            i+= 1
        elif parametros[i] == "change_color":
            original = [tuple(map(int, color.split(','))) for color in parametros[i + 1].split(':')]
            change = [tuple(map(int, color.split(','))) for color in parametros[i + 2].split(':')]
            image_trans = change_colors(image_trans, original, change)
            i+= 3

        elif parametros[i] == "shift":
            image_trans = shift(image_trans, int(parametros[i + 1]), int(parametros[i + 2]))
            i+= 3
        elif parametros[i] == "crop":
            image_trans = crop(image_trans, int(parametros[i+1]), int(parametros[i+2]), int(parametros[i+3]), int(parametros[i+4]))
            i+= 5
        elif parametros[i] == "filter":
            image_trans = filter(image_trans, float(parametros[i+1]), float(parametros[i+2]), float(parametros[i+3]))
            i+= 4
        else:
            print("Transformacion no valida")
            i+= 1



    file_name = sys.argv[3].replace(".", "_trans.")
    write_img(image_trans, file_name)

if __name__ == "__main__":
    main()




