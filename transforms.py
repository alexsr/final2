from images import *
def mirror(image: dict) -> dict:
    (width, height) = size(image)
    imagen_espejada = create_blank(width, height)
    for i in range(height * width):
        y, x = i // width , i % width
        imagen_espejada['pixels'][i] = image['pixels'][y * width + (width - 1 - x)]

    return imagen_espejada

def grayscale(image: dict) -> dict:
    (width, height) = size(image)
    imagen_grises = create_blank(width, height)
    for i in range(height * width):
        r,g,b = image['pixels'][i]
        gris = int((r+g+b)/3)
        imagen_grises['pixels'][i] = (gris, gris, gris)

    return imagen_grises

def blur(image: dict) -> dict:
    (width, height) = size(image)
    imagen_blur = create_blank(width, height)
    for i in range(height * width):
        y,x = i // width, i % width
        pixel_contiguo = []
        for dy in [-1,0,1]:
            for dx in [-1,0,1]:
                nx, ny = x + dx, y + dy
                if 0 <= nx < width and 0 <= ny < height:
                    pixel_contiguo.append(image['pixels'][ny * width + nx])
        media_r = sum(pixel[0] for pixel in pixel_contiguo)// len(pixel_contiguo)
        media_g = sum(pixel[1] for pixel in pixel_contiguo) // len(pixel_contiguo)
        media_b = sum(pixel[2] for pixel in pixel_contiguo)// len(pixel_contiguo)
        imagen_blur['pixels'][i] = (media_r, media_g, media_b)

    return imagen_blur

def change_colors(image: dict,
                  original: list[tuple[int, int, int]],
                  change: list[tuple[int, int, int]]) -> dict:
    (width, height) = size(image)
    imagen_colores_cambiados = create_blank(width, height)
    for i in range(height * width):
        r,g,b = image['pixels'][i]
        color_nuevo = (r,g,b)
        for j in range(len(original)):
            if (r,g,b) == original[j]:
                color_nuevo = change[j]
                break
        imagen_colores_cambiados['pixels'][i] = color_nuevo

    return imagen_colores_cambiados

def rotate(image: dict, direction: str) -> dict:
    (width, height) = size(image)
    if direction == 'right':
        imagen_rotada = create_blank(height, width)
        for y in range(height):
            for x in range(width):
                imagen_rotada['pixels'][x * height + (height - 1 - y)] = image['pixels'][y * width + x]

    elif direction == 'left':
        imagen_rotada = create_blank(height, width)
        for y in range(height):
            for x in range(width):
                imagen_rotada['pixels'][(width - 1 - x) * height + y] = image['pixels'][y * width + x]

    else:
        raise ValueError("Debes escribir right o left")

    return imagen_rotada

def shift(image: dict, horizontal: int = 0, vertical: int = 0) -> dict:
    (width, height) = size(image)
    nueva_anchura = width + horizontal
    nueva_altura = height + vertical
    imagen_movida = create_blank(nueva_anchura, nueva_altura)

    for i in range(height * width):
        y,x = i // width, i % width
        nueva_x = x + horizontal
        nueva_y = y + vertical
        if nueva_x < nueva_anchura and nueva_y < nueva_altura:
            imagen_movida['pixels'][nueva_y * nueva_anchura + nueva_x] = image['pixels'][i]

    return imagen_movida

def crop(image: dict, x: int, y: int, width: int, height: int) -> dict:
    (original_width, original_height) = size(image)
    imagen_recortada = create_blank(width, height)

    for i in range(height * width):
         crop_y, crop_x = i // width, i % width
         original_y, original_x = y + crop_y, x + crop_x
         if original_y < original_height and original_x < original_width:
             imagen_recortada['pixels'][i] = image['pixels'][original_y * original_width + original_x]

    return imagen_recortada

def filter(image: dict, r: float, g: float, b: float) -> dict:
    (width, height) = size(image)
    imagen_filtrada = create_blank(width, height)

    for i in range(height * width):
        original_r, original_g, original_b = image['pixels'][i]
        nueva_r = min(int(original_r * r), 255)
        nueva_g = min(int(original_g * g), 255)
        nueva_b = min(int(original_b * b), 255)
        imagen_filtrada['pixels'][i] = (nueva_r, nueva_g, nueva_b)


    return imagen_filtrada













