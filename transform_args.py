import sys
from transforms import change_colors, rotate, shift, crop, filter
from images import read_img, write_img

def main():
    if len(sys.argv) < 4:
        print("Uso: python transform_multi.py <image_file> <transformation> [<args>...]")
        return

    imagen = read_img(sys.argv[3])
    parametros = sys.argv[4:]

    i = 0
    while i < len(parametros):
        if parametros[i] == "change_colors":
            color_original = [tuple(map(int, color.split(','))) for color in parametros[i + 1].split(':')]
            color_change = [tuple(map(int, color.split(','))) for color in parametros[i + 2].split(':')]
            imagen = change_colors(imagen, color_original, color_change)
            i += 3

        elif parametros[i] == "rotate":
            direccion = parametros[i + 1]
            imagen = rotate(imagen, direccion)
            i += 2

        elif parametros[i] == "shift":
            horizontal = int(parametros[i + 1])
            vertical = int(parametros[i + 2])
            imagen = shift(imagen, horizontal, vertical)
            i += 3

        elif parametros[i] == "crop":
            x = int(parametros[i + 1])
            y = int(parametros[i + 2])
            width = int(parametros[i + 3])
            height = int(parametros[i + 4])
            imagen = crop(imagen, x, y, width, height)
            i += 5

        elif parametros[i] == "filter":
            r = float(parametros[i + 1])
            g = float(parametros[i + 2])
            b = float(parametros[i + 3])
            imagen = filter(imagen, r, g, b)
            i += 4

        else:
            print(f"Transformación no válida: {parametros[i]}")
            return

    file_name = sys.argv[3].replace(".", "_trans.")
    write_img(imagen, file_name)

if __name__ == '__main__':
    main()