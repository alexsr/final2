import sys
from images import read_img, write_img
from transforms import grayscale, mirror, blur
def main():
    image = read_img(sys.argv[3])

    if sys.argv[4] == "mirror":
        image_trans = mirror(image)
    elif sys.argv[4] == "grayscale":
        image_trans = grayscale(image)
    elif sys.argv[4] == "blur":
        image_trans = blur(image)
    else:
        print("Transformación no válida")

    file_name = sys.argv[3].replace(".", "_trans.")
    write_img(image_trans, file_name)

if __name__ == '__main__':
    main()
