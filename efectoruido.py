import random
from images import *
import sys
def ruido(image: dict, nivel_ruido: int) -> dict:

    (width, height) = size(image)
    imagen_ruidosa = create_blank(width, height)

    for i in range(width * height):
        r, g, b = image['pixels'][i]
        ruido_r = min(255, max(0, r + random.randint(-nivel_ruido, nivel_ruido)))
        ruido_g = min(255, max(0, g + random.randint(-nivel_ruido, nivel_ruido)))
        ruido_b = min(255, max(0, b + random.randint(-nivel_ruido, nivel_ruido)))
        imagen_ruidosa['pixels'][i] = (ruido_r, ruido_g, ruido_b)

    return imagen_ruidosa

def main():
    image_file = sys.argv[3]
    cantidad = int(sys.argv[4])

    imagen = read_img(image_file)
    imagen_con_ruido = ruido(imagen, cantidad)

    file_name = image_file.replace(".", "_ruido.")
    write_img(imagen_con_ruido, file_name)

if __name__ == '__main__':
    main()